<p align="center">
<img src="./doc/assets/logo.png" width="30%" >
</p>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.45.2-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-00%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>

## <img alt="" src="./doc/assets/readme-icon-introduction.png" style="display: inline-block;" width=3%/>介绍

参考 [serde](https://serde.rs/) 库，使用元编程自动实现 `Serializable` 接口。目前功能比较简单，当作抛砖引玉。

- `@DeriveSerializable` 宏
  - 自动实现 `Serializable` 接口，同时自动生成一个命名参数版 `init` 函数
  - 可以作为属性宏，也可以作为非属性宏
  - 属性宏：
    - `DeriveSerializable[gen_init=false]`: 不自动生成 `init` 函数
  - 目前 `@DeriveSerializable` 只支持 `class` 和 `struct`

- `@SerAttr` 宏
  - 配置序列化行为
  - `@SerAttr[rename="new_name"]`
    - 使用 `"new_name"` 序列化和反序列化本字段
  - `@SerAttr[rename(serialize="ser_name", deserialize="des_name")]`
    - 使用 `"ser_name"` 序列化本字段
    - 使用 `"des_name"` 反序列化本字段
  - `@SerAttr[skip]`
    - 序列化和反序列时，忽略本字段
    - `@SerAttr[skip_serialize]`
      - 序列化时，忽略本字段
    - `@SerAttr[skip_deserialize]`
      - 反序列化时，忽略本字段

- 调用方需要引入自动序列化依赖的包和类型：

  ```cangjie
  import serialization.serialization.{Serializable, DataModelStruct, DataModel, field}
  import encoding.json.{JsonValue, ToJson}
  ```

### 下一步

- 支持枚举类型的自动序列化
- 支持 `@SerAttr[rename_all]`：使用指定的命名规则重新命名所有字段，如 `snake_case` 命名规则。
- 支持 `@SerAttr[default="path"]`：设置默认值
- 支持 `@SerAttr[pkg_name="path"]`：支持设置`serialization` 和 `json` 的重命名包名。
- 支持泛型参数

### 源码目录

```shell
.
├── README.md
├── doc
├── module.json
├── modules
│   └── meta_item
├── src
│   ├── derive_serializable
│   │   ├── ast.cj
│   │   ├── attr.cj
│   │   ├── derive.cj
│   │   └── gen.cj
│   └── main.cj
└── test
    ├── HLT
    ├── LLT
    └── UT
```

- `doc`是库的设计文档、提案、库的使用文档
- `src`是库源码目录
- `test`是存放测试用例，包括HLT用例、LLT 用例和UT用例

### 接口说明

#### macro DeriveSerializable

自动实现序列化

```cangjie
public macro DeriveSerializable(input: Tokens): Tokens
public macro DeriveSerializable(attrs: Tokens, input: Tokens): Tokens
```

#### macro SerAttr

配置序列化属性

```cangjie
public macro SerAttr(attrs: Tokens, input: Tokens): Tokens
```

## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/>编译执行

### 编译

- 下载代码：记得同时下载依赖包 [meta_item](https://gitee.com/HW-PLLab/meta_item)

  ```shell
  git clone --recursive git@gitee.com:HW-PLLab/derive_serializable.git
  ```

- 编译
  ```shell
  cjpm clean
  cjpm build
  ```

- demo
  ```shell
  cd demo
  cjpm clean
  cjpm run
  ```

### 示例

```cangjie
import serde.{DeriveSerializable,SerAttr}
import serialization.serialization.*
import encoding.json.*

@DeriveSerializable
struct ProductTag {
    let name: String
    let color: String
}

@DeriveSerializable
struct Product {
    var name: String
    @SerAttr[rename="sku_id"]
    let skuId: String
    let image: String
    let price: String
    @SerAttr[rename="vip_price"]
    let vipPrice: String
    let mark: Int64
    let tags: Array<ProductTag>
}

main() {
    let product = Product(
        name: "productA",
        skuId: "123456",
        image: "https://www.baidu.com",
        price: "1234",
        vipPrice: "1000",
        mark: 1234,
        tags: [
            ProductTag(
                name: "智能手表",
                color: "#70AD47"
            ),
            ProductTag(
                name: "数码产品",
                color: "#4472C4"
            )
        ]
    )
    let jsonValue = product.serialize().toJson()
    println("jsonValue=${jsonValue}")

    var recovered = Product.deserialize(DataModel.fromJson(jsonValue))
    recovered.name = "recoveredProduct"
    let recoveredJsonValue = recovered.serialize().toJson()
    println("recoveredJsonValue=${recoveredJsonValue}")
}

```

执行结果如下：

```json
{"name":"productA","sku_id":"123456","image":"https://www.baidu.com","price":"1234","vip_price":"1000","mark":1234,"tags":[{"name":"智能手表","color":"#70AD47"},{"name":"数码产品","color":"#4472C4"}]}
```

```json
{"name":"recoveredProduct","sku_id":"123456","image":"https://www.baidu.com","price":"1234","vip_price":"1000","mark":1234,"tags":[{"name":"智能手表","color":"#70AD47"},{"name":"数码产品","color":"#4472C4"}]}
```

## <img alt="" src="./doc/assets/readme-icon-contribute.png" style="display: inline-block;" width=3%/>参与贡献

[@vargeek](https://gitee.com/vargeek)
